= Beneath the Leafless Tree
Matthew Harazim

== Prologue
[.lead] There is an unexplainable magical quality derived from the way the sun passes through the leaves of a tree.

The Japanese encapsulate this feeling in a single word; _Komorebi_, coming from the kanji characters for tree, shine through, and sun. English is not nearly as literal. We like to over-complicate matters, and instead of using the one singular word, we prefer the long-winded, poetic amalgamation of half the words in our dictionary to describe a singular phenomenon.

It was a rare amber-skied afternoon in October when these thoughts permeated through to the forefront of Lukas' mind. He sat hunched over a light-wooden desk, taking a welcome break from the four screens mounted on it by gazing out of the window to his right. Several metres past the confines of the window, a tree stood watch, the sunlight penetrating the diaphanous membrane of leaves, dancing in the wind.


